#!/usr/bin/env python3

import argparse
from subprocess import run, check_output
import os
import shutil
import yaml
import xml.etree.ElementTree as ET
import json

def create_xml(bucket_id, doc_name, version):
    entry = ET.Element("entry")
    ET.SubElement(entry, "version").text = version
    ET.SubElement(entry, "url").text = \
        "https://storage.googleapis.com/{bucket_id}/docs/{name}/{version}/{name}.tgz".format(
                bucket_id=bucket_id,
                name=doc_name,
                version=version)
    tree = ET.ElementTree(entry)
    return tree

def create_json_data(info_yaml, versions):
    latest_version = versions[0]

    data = dict()
    name = info_yaml["name"]
    data["name"] = name
    data["version"] = latest_version
    archive_name = name + ".tgz"
    data["archive"] = archive_name

    author_names = list()
    author_links = list()
    for author in info_yaml["authors"]:
        author_names.append(author["name"])
        author_links.append(author["link"])
    author_names_str = ", ".join(author_names)
    author_links_str = ", ".join(author_links)
    data["author"] = dict()
    data["author"]["name"] = author_names_str
    data["author"]["link"] = author_links_str

    data["aliases"] = info_yaml["aliases"]

    data["specific_versions"] = list()
    for version in versions:
        version_data = dict()
        version_data["version"] = version
        version_data["archive"] = "versions/" + version + "/" + archive_name
        data["specific_versions"].append(version_data)

    return data

def md_link(text, link):
    return "[{text}]({link})".format(text=text, link=link)

def generate_readme(info_yaml, doc_readme, f):
    name = info_yaml["name"]
    link = info_yaml["link"]

    # title
    f.write("# " + name + " Docset\n\n")

    # description
    md_name_link_text = md_link(name, link)
    description = info_yaml["description"]
    description = description.replace(name, md_name_link_text)
    f.write(description + "\n\n")

    # author(s)
    f.write("## ")
    authors = info_yaml["authors"]
    if len(authors) == 1:
        f.write("Author")
    else:
        f.write("Authors")
    f.write("\n\n")
    for author in authors:
        f.write("- " + md_link(author["name"], author["link"]))
    f.write("\n\n")

    # build instructions
    f.write("## How to build\n\n")
    gitlab_link = info_yaml["gitlab_link"]
    f.write(md_link("See here", gitlab_link))

def generate_and_upload_docs(config):
    bucket_id = config["bucket_id"]

    os.chdir("docs")
    for name in config["generate"]:
        versions_raw = config["generate"][name]
        versions = [str(v) for v in versions_raw]

        if len(versions) == 0:
            continue

        os.chdir(name)

        with open("info.yaml") as f:
            info_yaml = yaml.load(f, Loader=yaml.Loader)
        doc_name = info_yaml["name"]

        generate_cmd = "./generate.py --versions " + ' '.join(versions)
        run(generate_cmd.split())

        for version in versions:
            upload_cmd = "gcloud alpha storage cp versions/{version}/{name}.tgz gs://{bucket_id}/docs/{name}/{version}/{name}.tgz".format(
                    bucket_id=bucket_id,
                    name=doc_name,
                    version=version)
            print(upload_cmd)
            run(upload_cmd.split())

        os.chdir("..")

    os.chdir("..")

def generate_xml(config):
    bucket_id = config["bucket_id"]

    os.makedirs("xml", exist_ok=True)
    for name in config["xml"]:
        version_raw = config["xml"][name]
        version = str(version_raw)

        info_yaml_path = os.path.join("docs", name, "info.yaml")
        with open(info_yaml_path) as f:
            info_yaml = yaml.load(f, Loader=yaml.Loader)
        doc_name = info_yaml["name"]

        xml_tree = create_xml(bucket_id, doc_name, version)
        xml_tree.write("xml/{name}.xml".format(name=doc_name))
        upload_cmd = "gcloud alpha storage cp xml/{name}.xml gs://{bucket_id}/docs/{name}/{name}.xml".format(
                bucket_id=bucket_id,
                name=doc_name,
                version=version)
        run(upload_cmd.split())

def add_contributions(config):
    reset_all = config["contributions"]["reset_all"]
    if reset_all and os.path.exists("contributions"):
        shutil.rmtree("contributions")
    os.makedirs("contributions", exist_ok=True)
    os.chdir("contributions")

    for name in config["generate"]:
        info_yaml_path = os.path.join("../docs", name, "info.yaml")
        with open(info_yaml_path) as f:
            info_yaml = yaml.load(f, Loader=yaml.Loader)

        doc_name = info_yaml["name"]

        if os.path.exists(name):
            shutil.rmtree(name)
        os.makedirs(name)
        os.chdir(name)

        versions_raw = config["generate"][name]
        versions = [str(v) for v in versions_raw]

        if len(versions) == 0:
            continue

        if not os.path.exists("Dash-User-Contributions"):
            clone_contrib_repo_cmd = "git clone -b master --depth 1 https://github.com/Halmoni100/Dash-User-Contributions.git"
            run(clone_contrib_repo_cmd.split())
        os.chdir("Dash-User-Contributions")
        run("git pull".split())

        branch_name = "chong-" + name

        branches_str = check_output("git branch".split()) 
        branches_lines = branches_str.decode('UTF-8').split('\n')
        branches = set()
        for line in branches_lines:
            if len(line) > 2 and line[0:2] == "* ":
                branches.add(line[2:])
            else:
                branches.add(line)
        if branch_name in branches:
            raise RuntimeError("Branch exists for " + branch_name + ", should delete it first")
        else:
            checkout_new_cmd = "git checkout -b " + branch_name
            run(checkout_new_cmd.split())

        os.chdir("docsets")
        reset = config["contributions"][name]["reset"]
        if reset and os.path.exists(doc_name):
            shutil.rmtree(doc_name)
            os.mkdir(doc_name)
        if not os.path.exists(doc_name):
            os.mkdir(doc_name)
        os.chdir(doc_name)

        dashci_dir = "../../../../.."

        contrib_versions_raw = config["contributions"][name]["versions"]
        contrib_versions = [str(v) for v in contrib_versions_raw]
        json_data = create_json_data(info_yaml, contrib_versions)
        with open("docset.json", 'w') as f:
            json.dump(json_data, f, indent=4)

        doc_dir = os.path.join(dashci_dir, "docs", name)

        doc_readme_path = os.path.join(doc_dir, "README.md")
        with open(doc_readme_path) as f:
            doc_readme = f.read()
        with open("README.md", 'w') as f:
            generate_readme(info_yaml, doc_readme, f)

        set_icons = config["contributions"][name]["set_icons"]
        if set_icons:
            icon_dir = os.path.join(doc_dir, "icons")
            icon_path = os.path.join(icon_dir, "icon.png")
            icon2x_path = os.path.join(icon_dir, "icon@2x.png")
            shutil.copy(icon_path, "icon.png")
            shutil.copy(icon2x_path, "icon@2x.png")

        set_latest = config["contributions"][name]["set_latest"]
        docset_filename = doc_name + ".tgz"
        old_docset_ref = docset_filename + ".txt"
        if set_latest:
            latest_version = versions[0]
            if os.path.exists(old_docset_ref):
                os.remove(old_docset_ref)
            latest_docset_path = os.path.join(dashci_dir, "docs", name, "versions", latest_version, docset_filename)
            shutil.copy(latest_docset_path, docset_filename)

        os.makedirs("versions", exist_ok=True)
        for version in versions:
            version_dir = os.path.join("versions", version)
            os.makedirs(version_dir, exist_ok=True)
            docset_path = os.path.join(dashci_dir, "docs", name, "versions", version, docset_filename)
            if os.path.exists(os.path.join(version_dir, old_docset_ref)):
                os.remove(os.path.join(version_dir, old_docset_ref))
            shutil.copy(docset_path, os.path.join(version_dir, docset_filename))

        os.chdir("../../../..")

    os.chdir("..")

def main(config_path):
    with open(config_path) as f:
        config = yaml.load(f, Loader=yaml.Loader)

    generate_and_upload_docs(config)
    generate_xml(config)
    add_contributions(config)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='Dash Docs Generator',
        description='Generate dash docs')
    parser.add_argument("-c", "--config", default="configs/default.yaml", help="config file")
    args = parser.parse_args()
    main(args.config)
