FROM ubuntu:jammy

# general
RUN apt update && apt install -y wget python3 neovim tmux iputils-ping net-tools curl git make ca-certificates 
# gcloud
RUN apt install -y apt-transport-https ca-certificates gnupg curl

# pip
RUN mkdir -p /cots/pip
WORKDIR /cots/pip
RUN wget https://bootstrap.pypa.io/get-pip.py
RUN python3 get-pip.py
RUN pip install pynvim virtualenvwrapper pyyaml

# gcloud
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | gpg --dearmor -o /usr/share/keyrings/cloud.google.gpg
RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
RUN apt update && apt install google-cloud-cli

# conan docs
RUN apt install -y libenchant-2-2 graphviz

WORKDIR /root
COPY genall.py /root/genall.py
COPY docs /root/docs
COPY configs /root/configs
